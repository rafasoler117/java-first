package co.simplon.promo16.yo;

public class Validator {
    
    public int min;
    public int max;
    
    public Validator(int min, int max) {
        this.min = min;
        this.max = max;
    }
    public String isCorrectLength (String one)
    {
        if (one.length() >= 2 && one.length() <= 32)
            return "true";
        
            else
                return "false";
    }
    public String haveTrailingSpaces (String yo)
    {
        if (yo.startsWith(" ") || yo.endsWith(" "))
            return "true";
            
        else
            return "false";
    }
    public String haveSpecialChars (String hi)
    {
        if (hi.contains("-") ||hi.contains("_") || hi.contains("!"))
            return "true";
        else
            return "false";
    }
    public String validate (String check)
    {
        if (isCorrectLength(check) == "true" && haveTrailingSpaces(check) == "false" && haveSpecialChars(check) == "false")
            return "true";
        else
            return "false";
    }
    public Boolean isCorrectPassword (String mdp)
    {
        if (mdp.length()>4)
           return false;

        if (mdp.contains("[A-Z]") && mdp.contains("[0-9]"))
            return true;
        else
        return false;
       // char []tab = mdp.toCharArray();
//
       // if (mdp.length()>4)
       //     return false;
//
       // for (int i = 0;i < mdp.length(); i++)
       //     if (Character.isUpperCase(tab[i]) &&  )
       //         return true;
    }
}
