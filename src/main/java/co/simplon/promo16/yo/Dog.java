package co.simplon.promo16.yo;

public class Dog {
    public String name;
    public String breed;
    public int age;

    public Dog() {
    }

    public Dog(String name, String breed, int age) {
        this.name = name;
        this.breed = breed;
        this.age = age;
    }

    public void greeting()
    {
        System.out.println("Hello my name is " + name +" i'm a " + breed + 
            " and i'm " + age +" years old. Woof Woof");
    }
}
